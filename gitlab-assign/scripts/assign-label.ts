import axios, { AxiosRequestConfig } from "axios";
import { execSync } from "child_process";

const testDirs: [RegExp, string][] = [
  [/([1]).*/g, "Dir 1"],
  [/([2]).*/g, "Dir 2"],
  [/([3]).*/g, "Dir 3"],
  [/([4]).*/g, "Dir 4"],
  [/^package.json$/g, "dependencies"],
  [/([gitlab\-assign]).*/g, "gitlab"],
];

const gitlabServerHost = process.env.CI_SERVER_HOST;
const mergeRequestId = process.env.CI_MERGE_REQUEST_IID;
const mergeRequestTargetBranch =
  process.env.CI_MERGE_REQUEST_TARGET_BRANCH_NAME;

const GITLAB_HEADERS: AxiosRequestConfig["headers"] = {
  "PRIVATE-TOKEN": process.env.ACCESS_TOKEN,
};

function getLabelsToAdd(): string[] {
  const test = execSync(`git diff ${mergeRequestTargetBranch} --name-only`)
    .toString()
    .split("\n");
  const labelsToAdd = testDirs
    .filter((testDir) => test.find((t) => testDir[0].test(t)))
    .map((testDir) => testDir[1]);
  console.log(test);
  console.log(labelsToAdd);
  return labelsToAdd;
}

async function run() {
  const labelsToAdd = getLabelsToAdd();

  const test = await axios.put(
    `https://gitlab.com/api/v4/projects/59005302/merge_requests/${mergeRequestId}`,
    { add_labels: labelsToAdd.join(",") },
    { headers: GITLAB_HEADERS }
  );

  console.log(test);
}

run();
